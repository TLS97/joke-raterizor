import { useState } from "react";

const roflList = [];
const mehList = [];

function Joke() {
    const [joke, setJoke] = useState({});
    
    function getJoke() {
        fetch("https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single")
        .then(response => response.json())
        .then(result => 
            {
                setJoke(result);
            })
    }

    function upVote() {
        roflList.push(joke);
        getJoke();
        console.log(roflList);
    }
    function downVote() {
        mehList.push(joke)
        getJoke();
        console.log(mehList);
    }

    return (
        <div>
            <h4>{joke.joke}</h4>
            <button onClick={upVote}>🤣</button>
            <button onClick={downVote}>😟</button>
        </div>
    );
}

export default Joke;